const http = require("http");
const fs = require("fs");
const port = 3000;

const server = http.createServer(function (req, res) {
  if (req.url === "/html") {
    res.writeHead(200, { "content-Type": "text/html" });
    fs.readFile("./index.html", function (error, data) {
      if (error) {
        res.writeHead(404);
        res.write("error: File not found");
      } else {
        res.write(data);
      }
      res.end();
    });
  } else if (req.url === "/json") {
    res.writeHead(200, { "content-Type": "application/json" });
    fs.readFile("./file.json", function (error, data) {
      if (error) {
        res.writeHead(404);
        res.write("error: File not found");
      } else {
        res.write(data);
      }
      res.end();
    });
  } else if (req.url === "/uuid") {
    res.writeHead(200, { "content-Type": "text/plain" });
    fs.readFile("./file1.json", function (error, data) {
      if (error) {
        res.writeHead(404);
        res.write("error: File not found");
      } else {
        res.write(data);
      }
      res.end();
    });
  } else if (req.url.split("/")[1] === "status") {
    res.writeHead(Number(req.url.split("/")[2]), {
      "Content-Type": "text/plain",
    });
    res.end(`Success at ${req.url.split("/")[2]}`);
  }
  const path = req.url.split("/");
  if (path[1] === "delay") {
    const delay = Number(path[2]);
    setTimeout(() => {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end(`Success after ${delay} seconds of delay`);
    }, delay * 1000);
  }
});

server.listen(port, function (error) {
  if (error) {
    console.log("Something went wrong", error);
  } else {
    console.log("Server is listening on port " + port);
  }
});
